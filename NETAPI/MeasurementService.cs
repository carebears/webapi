﻿/* 
 * Copyright 2014 Aarhus University
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Collections.Generic;
using DTO;
using Newtonsoft.Json;

namespace NETAPI
{
    public class MeasurementService : IMeasurementService
    {
        private IWebApiProvider _webApiProvider;
        private string _apiUri = "api/measurements/";

        public MeasurementService(IWebApiProvider webApiProvider)
        {
            _webApiProvider = webApiProvider;
        }

        public List<T> Retrieve<T>() where T : Measurement
        {
            var retrievedJson = _webApiProvider.Get(_apiUri + typeof(T).Name);
            return JsonConvert.DeserializeObject<List<T>>(retrievedJson);
        }

        public void Add<T>(T newMeasurement) where T : Measurement
        {
            _webApiProvider.Post(_apiUri + typeof(T).Name, JsonConvert.SerializeObject(newMeasurement));
        }

        public void Modify<T>(T existingMeasurement) where T : Measurement
        {
            _webApiProvider.Put(_apiUri + typeof(T).Name, JsonConvert.SerializeObject(existingMeasurement));
        }

        public void Delete<T>(T existingMeasurement) where T : Measurement
        {
            _webApiProvider.Delete(_apiUri + typeof(T).Name);
        }
    }
}

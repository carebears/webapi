﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace NETAPI
{
    public class WebApiProvider : IWebApiProvider
    {
        private string _uriBase;
        private IAuthProvider _authProvider;

        public WebApiProvider(string uriBase, IAuthProvider authProvider)
        {
            _uriBase = uriBase;
            _authProvider = authProvider;
        }

        public string Get(string uriExtension)
        {
            var contentStream = new MemoryStream();
            var webRequest = (HttpWebRequest)WebRequest.Create(_uriBase + uriExtension);
            webRequest.Method = "GET";

            _authProvider.ApplyAuth(webRequest);

            using (var response = webRequest.GetResponse())
            {
                using (var responseStream = response.GetResponseStream())
                {
                    responseStream.CopyTo(contentStream);
                }
            }

            contentStream.Position = 0;
            var streamReader = new StreamReader(contentStream);
            return streamReader.ReadToEnd();
        }

        public void Post(string uriExtension, string resource)
        {
            var content = new MemoryStream();
            var webRequest = (HttpWebRequest)WebRequest.Create(_uriBase + uriExtension);
            webRequest.Method = "POST";
            webRequest.ContentType = "application/json";
            _authProvider.ApplyAuth(webRequest);

            using (var stream = webRequest.GetRequestStream())
            {
                var resourceArray = Encoding.UTF8.GetBytes(resource);
                stream.Write(resourceArray, 0, resourceArray.Length);
                stream.Close();

                using (var response = webRequest.GetResponse())
                {
                    using (var responseStream = response.GetResponseStream())
                    {
                        responseStream.CopyTo(content);
                    }
                }
            }
           
            //Handling if not 200
        }

        public void Put(string uriExtension, string resource)
        {
            var content = new MemoryStream();
            var webRequest = (HttpWebRequest)WebRequest.Create(_uriBase + uriExtension);
            webRequest.Method = "PUT";
            webRequest.ContentType = "application/json";
            _authProvider.ApplyAuth(webRequest);

            using (var stream = webRequest.GetRequestStream())
            {
                var resourceArray = Encoding.UTF8.GetBytes(resource);
                stream.Write(resourceArray, 0, resourceArray.Length);
                stream.Close();

                using (var response = webRequest.GetResponse())
                {
                    using (var responseStream = response.GetResponseStream())
                    {
                        responseStream.CopyTo(content);
                    }
                }
            }

            //Handling if not 200
        }

        public void Delete(string uriExtension)
        {
            var content = new MemoryStream();
            var webRequest = (HttpWebRequest)WebRequest.Create(_uriBase + uriExtension);
            webRequest.Method = "DELETE";
            webRequest.ContentType = "application/json";
            _authProvider.ApplyAuth(webRequest);

            using (var response = webRequest.GetResponse())
            {
                using (var responseStream = response.GetResponseStream())
                {
                    responseStream.CopyTo(content);
                }
            }

            //Handling if not 200
        }

       
    }
}

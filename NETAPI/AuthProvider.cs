﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace NETAPI
{
    public class AuthProvider : IAuthProvider
    {
        private string _baseUri;
        private string _username;
        private string _password;
        private DateTime _authExpires;
        private string _authAccessToken;
        private string _authTokenType;

        public AuthProvider(string baseUri, string username, string password)
        {
            _baseUri = baseUri;
            _username = username;
            _password = password;
            _authExpires = DateTime.Now;
            _authAccessToken = "";
            _authTokenType = "";

        }

        public void ApplyAuth(WebRequest webRequest)
        {
            if (_authExpires.CompareTo(DateTime.Now) < 0)
                RetrieveToken();

            webRequest.Headers.Add("Authorization", _authTokenType + " " + _authAccessToken);
        }

        public void RetrieveToken()
        {
            var contentStream = new MemoryStream();
            var webRequest = (HttpWebRequest) WebRequest.Create(_baseUri + "token");
            webRequest.Method = "POST";

            string postData = "grant_type=password&username=" + _username + "&password=" + _password;
            byte[] byteArray = Encoding.UTF8.GetBytes(postData);
            webRequest.ContentType = "application/x-www-form-urlencoded";
            webRequest.ContentLength = byteArray.Length;
            webRequest.Accept = "application/json, text/plain";

            using (var stream = webRequest.GetRequestStream())
            {
                stream.Write(byteArray, 0, byteArray.Length);
                stream.Close();

                using (var response = webRequest.GetResponse())
                {
                    using (var responseStream = response.GetResponseStream())
                    {
                        responseStream.CopyTo(contentStream);
                    }
                }
            }

            contentStream.Position = 0;
            var streamReader = new StreamReader(contentStream);
            var authString = streamReader.ReadToEnd();
            var authData = JObject.Parse(authString);
            _authExpires = DateTime.Now.AddSeconds((int)authData["expires_in"]);
            _authAccessToken = (string) authData["access_token"];
            _authTokenType = (string) authData["token_type"];
        }

       
    }
}

﻿/* 
 * Copyright 2014 Aarhus University
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System.Security.Claims;
using System.Threading.Tasks;
using DTO;
using Microsoft.Owin.Security.OAuth;
using WebAPI.Security;

namespace WebAPI.Providers
{
    public class SimpleAuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });

            using (var userRepo = new AuthRepository<User>())
            using (var patientRepo = new AuthRepository<Patient>())
            using (var hcpRepo = new AuthRepository<HealthcareProfessional>())
            {
                var user = await userRepo.FindUser(context.UserName, context.Password);
                if (user == null) user = await patientRepo.FindUser(context.UserName, context.Password);
                if (user == null) user = await hcpRepo.FindUser(context.UserName, context.Password);
                
                if (user == null)
                {
                    context.SetError("invalid_grant", "The user name or password is incorrect.");
                    return;
                }

                var identity = new ClaimsIdentity(context.Options.AuthenticationType);
                identity.AddClaim(new Claim("id", user.Id));
                identity.AddClaim(new Claim("username", context.UserName));
                identity.AddClaim(new Claim("role", user.Roles[0]));
                identity.AddClaim(new Claim("fullname", user.GivenName + " " + user.Surname));
                context.Validated(identity);
            }

            
            
            
        }
    }
}
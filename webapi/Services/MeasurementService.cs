﻿/* 
 * Copyright 2014 Aarhus University
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System.Collections.Generic;
using System.Linq;
using Database;
using DTO;

namespace WebAPI.Services
{
    public class MeasurementService : IMeasurementService
    {

        #region Private Fields
        private LogService logs;
        private UserService<User> user;
        private IDatabaseHandler<Measurement> databaseHandler;
        private string currentUserId;        
        #endregion

        #region Constructors
        public MeasurementService(string userId, string userFullName)
        {
            currentUserId = userId;
            logs = new LogService(userId, userFullName);       
        }
        #endregion

        public List<T> GetPatientMeasurements<T>(string patientId) where T : Measurement
        {
            IDatabaseHandler<T> databaseHandler = new DatabaseHandler<T>();
            IList<T> measurements = databaseHandler.SearchFor(x => x.GetType() == typeof(T) && x.PatientId.ToString() == patientId);

            logs.LogReadList(measurements.FirstOrDefault());
            return measurements.ToList();
        }

        public bool Create<T>(T measurement) where T : Measurement
        {
            DatabaseHandler<T> databaseHandler = new DatabaseHandler<T>();
            measurement.CreatedBy = currentUserId;
            bool measurementWasInserted = databaseHandler.Insert(measurement);
            if (measurementWasInserted)
            {
                logs.DoLog(measurement, "Created");
            }
            return measurementWasInserted;
        }

        public bool Update<T>(T measurement) where T : Measurement
        {
            DatabaseHandler<T> databaseHandler = new DatabaseHandler<T>();
            bool measurementWasUpdated = databaseHandler.Update(measurement);
            if (measurementWasUpdated)
            {
                logs.DoLog(measurement, "Updated");
            }
            return measurementWasUpdated;
        }

        public bool Delete<T>(string measurementId) where T : Measurement
        {
            DatabaseHandler<T> databaseHandler = new DatabaseHandler<T>();
            var resultList = databaseHandler.SearchFor(x => x.GetType() == typeof(T) && x.Id.ToString() == measurementId);
            var objectToDelete = resultList.FirstOrDefault();                                                            
            bool measurementWasDeleted = databaseHandler.Delete(objectToDelete);
            if (measurementWasDeleted)
            {
                logs.DoLog(objectToDelete, "Deleted");
            }
            return measurementWasDeleted;
        }

        public string GetMeasurementOwnerId<T>(string measurementId) where T : Measurement
        {
            var databaseHandler = new DatabaseHandler<T>();
            var foundMeasurement =
                databaseHandler.SearchFor(x => x.GetType() == typeof (T) && x.Id.ToString() == measurementId).FirstOrDefault();

            return foundMeasurement.PatientId;
        }
    }
}
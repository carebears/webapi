﻿/* 
 * Copyright 2014 Aarhus University
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System.Collections.Generic;
using System.Linq;
using Database;
using DTO;

namespace WebAPI.Services
{
    public class TrackingService : ITrackingService
    {
        #region Private members

        private Database.DatabaseHandler<Patient> patientDatabaseHandler;
        private Database.DatabaseHandler<HealthcareProfessional> healthcareProfessionalDatabaseHandler; 
        #endregion  

        #region Constructors
        public TrackingService()
        {
            patientDatabaseHandler = new DatabaseHandler<Patient>();
            healthcareProfessionalDatabaseHandler = new DatabaseHandler<HealthcareProfessional>();
        }
        #endregion 

        #region Public Methods
        
        #region Security
        public void CreatePassphrase(string userId, string passphrase)
        {
            var user = patientDatabaseHandler.GetById(userId);
            user.Passphrase = passphrase;
            patientDatabaseHandler.Update(user);
        }
        #endregion

        #region Healthcare Professionals
        public List<HealthcareProfessional> GetTrackingHealthcareProfessionals(string patientId)
        {
            return healthcareProfessionalDatabaseHandler.SearchFor(h => h.Patients.Contains(patientId)) as List<HealthcareProfessional>;
        }
        #endregion

        #region Patients
        public Patient GetPatientById(string patientId)
        {
            return patientDatabaseHandler.SearchFor(p => p.Id == patientId).FirstOrDefault();
        }

        public List<Patient> GetTrackedPatients(string healthCareProfessionalId)
        {
            var healthCareProfessional =
               healthcareProfessionalDatabaseHandler.SearchFor(x => x.Id == healthCareProfessionalId).FirstOrDefault();
            var patients = new List<Patient>();

            if (healthCareProfessional.Patients == null)
                return patients;

            foreach (string patientId in healthCareProfessional.Patients)
            {
                var patient = patientDatabaseHandler.SearchFor(p => p.Id.Equals(patientId)).FirstOrDefault();

                patients.Add(patient);
            }

            return patients;
        }

        public bool AddPatientTracking(string healthcareProfessionalId, string patientEmail, string patientPassphrase)
        {
            HealthcareProfessional healthcareProfessional =
                healthcareProfessionalDatabaseHandler.SearchFor(x => x.Id == healthcareProfessionalId).FirstOrDefault();

            if (healthcareProfessional == null)
                return false;

            var patientToTrack =
                patientDatabaseHandler.SearchFor(p => p.Email.Equals(patientEmail) &&
                   p.Passphrase.Equals(patientPassphrase)).ToList().FirstOrDefault();

            if (patientToTrack == null)
                return false;

            healthcareProfessional.Patients.Add(patientToTrack.Id);
            var isUpdated = healthcareProfessionalDatabaseHandler.Update(healthcareProfessional);

            return isUpdated;
        }

        public bool RemovePatientTracking(string healthcareProfessionalId, string patientId)
        {
            HealthcareProfessional healthcareProfessional =
               healthcareProfessionalDatabaseHandler.SearchFor(x => x.Id == healthcareProfessionalId).FirstOrDefault();

            if (healthcareProfessional == null)
                return false;

            if (!healthcareProfessional.Patients.Remove(patientId))
                return false;

            var isUpdated = healthcareProfessionalDatabaseHandler.Update(healthcareProfessional);

            return isUpdated;
        }
        #endregion

        #endregion

    }
}
﻿/* 
 * Copyright 2014 Aarhus University
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Database;
using DTO;

namespace WebAPI.Services
{
    public class LogService : ILogService
    {
        #region Private Fields
        private IDatabaseHandler<LogItem> databaseHandler;
        private string currentUserId;
        private string currentUserFullName;
        #endregion

        #region Constructors
        public LogService(string userId, string userFullName)
        {
            databaseHandler = new DatabaseHandler<LogItem>();
            currentUserId = userId;
            currentUserFullName = userFullName;
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Create new log item with viewed as Action and persist it in the database
        /// </summary>
        /// <param name="measurementType"></param>        
        public void LogReadList(Measurement measurementType)
        {
            if (measurementType != null)
            {
                LogItem logItem = new LogItem();
                databaseHandler = new DatabaseHandler<LogItem>();
                DateTime time = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Local);
                logItem.Date = new DateTime(time.Year, time.Month, time.Day, time.Hour, time.Minute, time.Second);
                logItem.Action = "Viewed";
                logItem.UserId = currentUserId;
                logItem.measurementId = measurementType.Id;
                logItem.MeasurementOwnerId = measurementType.PatientId;
                logItem.MeasurementType = measurementType.GetType().ToString().Replace("DTO.", "");
                logItem.UserModifyedBy = currentUserFullName;
                databaseHandler.Insert(logItem);
            }
        }

        /// <summary>
        /// Create a new log item with an enum Actiontype, should be either "Created", "Updated" or "Deleted"
        /// </summary>
        /// <param name="measurement">The measurement the CRUD action takes place on</param>
        /// <param name="action">CRUD action</param>
        public void DoLog(Measurement measurement, string action)
        {
            if (measurement != null)
            {
                LogItem logItem = new LogItem();
                databaseHandler = new DatabaseHandler<LogItem>();
                DateTime time = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Local);
                logItem.Date = new DateTime(time.Year, time.Month, time.Day, time.Hour, time.Minute, time.Second);
                logItem.Action = action;
                logItem.UserId = currentUserId;
                logItem.measurementId = measurement.Id;
                logItem.MeasurementOwnerId = measurement.PatientId;
                logItem.MeasurementType = measurement.GetType().ToString().Replace("DTO.", "");
                logItem.UserModifyedBy = currentUserFullName;
                databaseHandler.Insert(logItem);
            }
        }

        /// <summary>
        /// Gets all logi tems that belongs to current user that is younger then the specified "maxLogAge"
        /// </summary>
        /// <param name="maxLogAge">int representing the max age of the log items</param>
        /// <returns></returns>
        public List<LogItem> GetAllUserLogs(Int32 maxLogAge)
        {
            IList<LogItem> logItemList = databaseHandler.SearchFor(x => x.GetType() == typeof(LogItem) && x.MeasurementOwnerId == currentUserId && ((DateTime.Now - x.Date).TotalDays < maxLogAge));
            return logItemList.ToList();
        }

        /// <summary>
        /// Deletes all logs that belongs to the current user
        /// </summary>
        public void DeleteAllLogs()
        {
            var databaseHandler = new DatabaseHandler<LogItem>();
            IList<LogItem> logItemList = databaseHandler.SearchFor(x => x.GetType() == typeof(LogItem) && x.UserId == currentUserId);            
            foreach (var logItem in logItemList)
            {
               databaseHandler.Delete(new LogItem() { Id = logItem.Id });
            }            
        }
        #endregion
    }
}
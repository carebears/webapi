﻿/* 
 * Copyright 2014 Aarhus University
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System.Collections.Generic;
using DTO;

namespace WebAPI.Services
{
    interface ITrackingService
    {
        void CreatePassphrase(string userId, string passphrase);
        List<HealthcareProfessional> GetTrackingHealthcareProfessionals(string userId);
        Patient GetPatientById(string patientId);
        List<Patient> GetTrackedPatients(string healthCareProfessionalId);
        bool AddPatientTracking(string healthcareProfessionalId, string patientEmail, string patientPassphrase);
        bool RemovePatientTracking(string healthcareProfessionalId, string patientId);
    }
}

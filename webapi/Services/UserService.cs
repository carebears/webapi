﻿/* 
 * Copyright 2014 Aarhus University
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System.Linq;
using Database;
using DTO;
using Microsoft.AspNet.Identity;
using WebAPI.Security;

namespace WebAPI.Services
{
    public class UserService<T> : IUserService<T> where T : User 
    {

        #region Private Fields

        private IDatabaseHandler<T> databaseHandler;
        private AuthRepository<T> authRepo; 

        #endregion

        public UserService()
        {
            authRepo = new AuthRepository<T>();
            databaseHandler = new DatabaseHandler<T>();
        }

        public IdentityResult Create(T user)
        {
            return authRepo.RegisterUser(user, user.Password);
        }

        public T Read(string userId)
        {
            return databaseHandler.SearchFor(x => x.Id.Equals(userId)).FirstOrDefault();
        }

        public bool Update(T user)
        {
            return databaseHandler.Update(user);
        }

        public bool Delete(string userId)
        {
            var user = Read(userId);
            return databaseHandler.Delete(user);
        }

        public bool IsAuthorizedToAccess(string requesterId, string ownerId)
        {
            if (ownerId == null || requesterId == null)
                return false;

            if (requesterId == ownerId)
                return true;

            var hcpService = new UserService<HealthcareProfessional>();

            var hcp = hcpService.Read(requesterId);

            if (hcp == null)
                return false;

            if (!hcp.Patients.Contains(ownerId))
                return false;
            
           
            return true;
        }
    }
}
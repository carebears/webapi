﻿/* 
 * Copyright 2014 Aarhus University
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Web.Http;
using DTO;
using DTO.Measurements;
using WebAPI.Services;

namespace WebAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/measurements")]
    public class MeasurementsController : ApiController
    {

        #region Private Fields

        private Claim idClaim;
        private Claim roleClaim;
        private Claim fullNameClaim;
        private IMeasurementService measurementService;
        private IUserService<User> userService;

        #endregion

        #region Constructors

        MeasurementsController()
        {
            var identity = User.Identity as ClaimsIdentity;
            idClaim = identity.Claims.FirstOrDefault(c => c.Type == "id");
            roleClaim = identity.Claims.FirstOrDefault(c => c.Type == "role");
            fullNameClaim = identity.Claims.FirstOrDefault(c => c.Type == "fullname");
            measurementService = new MeasurementService(idClaim.Value, fullNameClaim.Value);
            userService = new UserService<User>();
        }

        #endregion

        #region Public Methods

        #region Bloodglucose
         
        [HttpGet]
        [Route("bloodglucose")]
        public IHttpActionResult GetAllBloodglucose([FromUri] string patientId = null)
        {
            if (roleClaim.Value == "patient")
                patientId = idClaim.Value;

            if (!userService.IsAuthorizedToAccess(idClaim.Value, patientId))
                return Unauthorized();

            var measurements = measurementService.GetPatientMeasurements<BloodGlucose>(patientId);
            if (measurements != null)
            {
                return Ok(measurements);
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        [HttpPost]
        [Route("bloodglucose")]
        public IHttpActionResult PostBloodglucose(BloodGlucose measurement, [FromUri] string patientId = null)
        {
            if (roleClaim.Value == "patient")
                patientId = idClaim.Value;

            if (!userService.IsAuthorizedToAccess(idClaim.Value, patientId))
                return Unauthorized();

            measurement.PatientId = patientId;

            var measurementWasInserted = measurementService.Create<BloodGlucose>(measurement);
            if (measurementWasInserted)
            {                
                return Ok();
            }
            return StatusCode(HttpStatusCode.BadRequest);
        }

        [HttpDelete]
        [Route("bloodglucose/{id}")]
        public IHttpActionResult DeleteBloodGlucose(string id)
        {
            var measurementOwnerId = measurementService.GetMeasurementOwnerId<BloodGlucose>(id);

            if (!userService.IsAuthorizedToAccess(idClaim.Value, measurementOwnerId))
                return Unauthorized();

            bool measurementWasDeleted = measurementService.Delete<BloodGlucose>(id);
            if (measurementWasDeleted)
            {                
                return Ok();
            }
            
            return StatusCode(HttpStatusCode.BadRequest);
        }

        [HttpPut]
        [Route("bloodglucose")]
        public IHttpActionResult UpdateBloodGlucose(BloodGlucose measurement)
        {
            if (!userService.IsAuthorizedToAccess(idClaim.Value, measurement.PatientId))
                return Unauthorized();

            bool measurementWasUpdated = measurementService.Update<BloodGlucose>(measurement);
            if (measurementWasUpdated)
            {                
                return Ok();
            }
            
            return StatusCode(HttpStatusCode.BadRequest);
        }

        #endregion

        #region Bloodpressure

        [HttpPost]
        [Route("bloodpressure")]
        public IHttpActionResult PostBloodPressure(BloodPressure measurement, [FromUri] string patientId = null)
        {
            if (roleClaim.Value == "patient")
                patientId = idClaim.Value;

            if (!userService.IsAuthorizedToAccess(idClaim.Value, patientId))
                return Unauthorized();

            measurement.PatientId = patientId;
            var measurementWasInserted = measurementService.Create<BloodPressure>(measurement);
            if (measurementWasInserted)
            {                
                return Ok();
            }
            return StatusCode(HttpStatusCode.BadRequest);
        }

        [HttpGet]
        [Route("bloodpressure")]
        public IHttpActionResult GetAllBloodpressure([FromUri] string patientId = null)
        {
            if (roleClaim.Value == "patient")
                patientId = idClaim.Value;

            if (!userService.IsAuthorizedToAccess(idClaim.Value, patientId))
                return Unauthorized();

            var measurements = measurementService.GetPatientMeasurements<BloodPressure>(patientId);
            if (measurements != null)
            {
                return Ok(measurements);
            }
            
            return StatusCode(HttpStatusCode.NoContent);
        }

        [HttpDelete]
        [Route("bloodpressure/{id}")]
        public IHttpActionResult DeleteBloodPressure(string id)
        {
            var measurementOwnerId = measurementService.GetMeasurementOwnerId<BloodPressure>(id);

            if (!userService.IsAuthorizedToAccess(idClaim.Value, measurementOwnerId))
                return Unauthorized();

            bool measurementWasDeleted = measurementService.Delete<BloodPressure>(id);
            if (measurementWasDeleted)
            {                
                return Ok();
            }
           
            return StatusCode(HttpStatusCode.BadRequest);
        }

        [HttpPut]
        [Route("bloodpressure")]
        public IHttpActionResult UpdateBloodPressure(BloodPressure measurement)
        {
            if (!userService.IsAuthorizedToAccess(idClaim.Value, measurement.PatientId))
                return Unauthorized();

            bool measurementWasUpdated = measurementService.Update<BloodPressure>(measurement);
            if (measurementWasUpdated)
            {                
                return Ok();
            }
            
            return StatusCode(HttpStatusCode.BadRequest);
        }

        #endregion

        #region Weight

        [HttpPost]
        [Route("weight")]
        public IHttpActionResult PostWeight(Weight measurement, [FromUri] string patientId = null)
        {
            if (roleClaim.Value == "patient")
                patientId = idClaim.Value;

            if (!userService.IsAuthorizedToAccess(idClaim.Value, patientId))
                return Unauthorized();

            measurement.PatientId = patientId;
            var measurementWasInserted = measurementService.Create<Weight>(measurement);
            if (measurementWasInserted)
            {                
                return Ok();
            }
            return StatusCode(HttpStatusCode.BadRequest);
        }

        [HttpGet]
        [Route("weight")]
        public IHttpActionResult GetAllWeight([FromUri] string patientId = null)
        {
            if (roleClaim.Value == "patient")
                patientId = idClaim.Value;

            if (!userService.IsAuthorizedToAccess(idClaim.Value, patientId))
                return Unauthorized();

            var measurements = measurementService.GetPatientMeasurements<Weight>(patientId); 
            if (measurements != null)
            {                
                return Ok(measurements);
            }
           
            return StatusCode(HttpStatusCode.NoContent);
        }

        [HttpDelete]
        [Route("weight/{id}")]
        public IHttpActionResult DeleteWeight(string id)
        {
            var measurementOwnerId = measurementService.GetMeasurementOwnerId<Weight>(id);

            if (!userService.IsAuthorizedToAccess(idClaim.Value, measurementOwnerId))
                return Unauthorized();

            bool measurementWasDeleted = measurementService.Delete<Weight>(id);
            if (measurementWasDeleted)
            {                
                return Ok();
            }
            
            return StatusCode(HttpStatusCode.BadRequest);
        }

        [HttpPut]
        [Route("weight")]
        public IHttpActionResult UpdateWeight(Weight measurement)
        {
            if (!userService.IsAuthorizedToAccess(idClaim.Value, measurement.PatientId))
                return Unauthorized();

            bool measurementWasUpdated = measurementService.Update<Weight>(measurement);
            if (measurementWasUpdated)
            {                
                return Ok();
            }

            return StatusCode(HttpStatusCode.BadRequest);
        }

        #endregion

        #region Temperature

        [HttpPost]
        [Route("temperature")]
        public IHttpActionResult PostTemperature(Temperature measurement, [FromUri] string patientId = null)
        {
            if (roleClaim.Value == "patient")
                patientId = idClaim.Value;

            if (!userService.IsAuthorizedToAccess(idClaim.Value, patientId))
                return Unauthorized();

            measurement.PatientId = patientId;
            var measurementWasInserted = measurementService.Create<Temperature>(measurement);
            if (measurementWasInserted)
            {                
                return Ok();
            }
            return StatusCode(HttpStatusCode.BadRequest);
        }

        [HttpGet]
        [Route("temperature")]
        public IHttpActionResult GetAllTemperature([FromUri] string patientId = null)
        {
            if (roleClaim.Value == "patient")
                patientId = idClaim.Value;

            if (!userService.IsAuthorizedToAccess(idClaim.Value, patientId))
                return Unauthorized();

            var measurements = measurementService.GetPatientMeasurements<Temperature>(patientId);
            if (measurements != null)
            {
                return Ok(measurements);
            }
            
            return StatusCode(HttpStatusCode.NoContent);
        }

        [HttpDelete]
        [Route("temperature/{id}")]
        public IHttpActionResult DeleteTemperature(string id)
        {
            var measurementOwnerId = measurementService.GetMeasurementOwnerId<Temperature>(id);

            if (!userService.IsAuthorizedToAccess(idClaim.Value, measurementOwnerId))
                return Unauthorized();

            bool measurementWasDeleted = measurementService.Delete<Temperature>(id);
            if (measurementWasDeleted)
            {                
                return Ok();
            }

            return StatusCode(HttpStatusCode.BadRequest);
        }

        [HttpPut]
        [Route("temperature")]
        public IHttpActionResult UpdateTemperature(Temperature measurement)
        {
            if (!userService.IsAuthorizedToAccess(idClaim.Value, measurement.PatientId))
                return Unauthorized();

            bool measurementWasUpdated = measurementService.Update<Temperature>(measurement);
            if (measurementWasUpdated)
            {                
                return Ok();
            }
            
            return StatusCode(HttpStatusCode.BadRequest);
        }

        #endregion

        #region Saturation

        [Route("saturation")]
        [HttpPost]
        public IHttpActionResult PostSaturation(Saturation measurement, [FromUri] string patientId = null)
        {
            if (roleClaim.Value == "patient")
                patientId = idClaim.Value;

            if (!userService.IsAuthorizedToAccess(idClaim.Value, patientId))
                return Unauthorized();

            measurement.PatientId = patientId;

            var measurementWasInserted = measurementService.Create<Saturation>(measurement);
            if (measurementWasInserted)
            {
                return Ok();
            }
            return StatusCode(HttpStatusCode.BadRequest);
        }

        [HttpGet]
        [Route("saturation")]
        public IHttpActionResult GetAllSaturation([FromUri] string patientId = null)
        {
            if (roleClaim.Value == "patient")
                patientId = idClaim.Value;

            if (!userService.IsAuthorizedToAccess(idClaim.Value, patientId))
                return Unauthorized();

            var measurements = measurementService.GetPatientMeasurements<Saturation>(patientId);
            if (measurements != null)
            {
                return Ok(measurements);
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        [HttpDelete]
        [Route("saturation/{id}")]
        public IHttpActionResult DeleteSaturation(string id)
        {
            var measurementOwnerId = measurementService.GetMeasurementOwnerId<Saturation>(id);

            if (!userService.IsAuthorizedToAccess(idClaim.Value, measurementOwnerId))
                return Unauthorized();

            bool measurementWasDeleted = measurementService.Delete<Saturation>(id);
            if (measurementWasDeleted)
            {                
                return Ok();
            }
            
            return StatusCode(HttpStatusCode.BadRequest);
        }

        [HttpPut]
        [Route("saturation")]
        public IHttpActionResult UpdateSaturation(Saturation measurement)
        {
            if (!userService.IsAuthorizedToAccess(idClaim.Value, measurement.PatientId))
                return Unauthorized();

            bool measurementWasUpdated = measurementService.Update<Saturation>(measurement);
            if (measurementWasUpdated)
            {                
                return Ok();
            }
            
            return StatusCode(HttpStatusCode.BadRequest);
        }

        #endregion

        #region ActivityEvent

        [Route("activityevent")]
        [HttpPost]
        public IHttpActionResult PostActivityEvent(ActivityEvent measurement, [FromUri] string patientId = null)
        {
            if (roleClaim.Value == "patient")
                patientId = idClaim.Value;

            if (!userService.IsAuthorizedToAccess(idClaim.Value, patientId))
                return Unauthorized();

            measurement.PatientId = patientId;

            var measurementWasInserted = measurementService.Create<ActivityEvent>(measurement);
            if (measurementWasInserted)
            {                
                return Ok();
            }

            return StatusCode(HttpStatusCode.BadRequest);
        }

        [HttpGet]
        [Route("activityevent")]
        public IHttpActionResult GetAllActivityEvent([FromUri] string patientId = null)
        {
            if (roleClaim.Value == "patient")
                patientId = idClaim.Value;

            if (!userService.IsAuthorizedToAccess(idClaim.Value, patientId))
                return Unauthorized();

            var measurements = measurementService.GetPatientMeasurements<ActivityEvent>(patientId); 
            if (measurements != null)
            {
                return Ok(measurements);
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        [HttpDelete]
        [Route("activityevent/{id}")]
        public IHttpActionResult DeleteActivityEvent(string id)
        {
            var measurementOwnerId = measurementService.GetMeasurementOwnerId<ActivityEvent>(id);

            if (!userService.IsAuthorizedToAccess(idClaim.Value, measurementOwnerId))
                return Unauthorized();

            bool measurementWasDeleted = measurementService.Delete<ActivityEvent>(id);
            if (measurementWasDeleted)
            {                
                return Ok();
            }
            
            return StatusCode(HttpStatusCode.BadRequest);
        }

        [HttpPut]
        [Route("activityevent")]
        public IHttpActionResult UpdateActivityEvent(ActivityEvent measurement)
        {
            if (!userService.IsAuthorizedToAccess(idClaim.Value, measurement.PatientId))
                return Unauthorized();
            
            bool measurementWasUpdated = measurementService.Update<ActivityEvent>(measurement);
            if (measurementWasUpdated)
            {                
                return Ok();
            }
            
            return StatusCode(HttpStatusCode.BadRequest);
        }

        #endregion

        #region Height

        [Route("height")]
        [HttpPost]
        public IHttpActionResult PostHeight(Height measurement, [FromUri] string patientId = null)
        {
            if (roleClaim.Value == "patient")
                patientId = idClaim.Value;

            if (!userService.IsAuthorizedToAccess(idClaim.Value, patientId))
                return Unauthorized();

            measurement.PatientId = patientId;
            var measurementWasInserted = measurementService.Create<Height>(measurement);
            if (measurementWasInserted)
            {                
                return Ok();
            }

            return StatusCode(HttpStatusCode.BadRequest);
        }

        [HttpGet]
        [Route("height")]
        public IHttpActionResult GetAllHeight([FromUri] string patientId = null)
        {
            if (roleClaim.Value == "patient")
                patientId = idClaim.Value;

            if (!userService.IsAuthorizedToAccess(idClaim.Value, patientId))
                return Unauthorized();

            var measurements = measurementService.GetPatientMeasurements<Height>(patientId); 
            if (measurements != null)
            {
                return Ok(measurements);
            }
            
            return StatusCode(HttpStatusCode.NoContent);
        }

        [HttpDelete]
        [Route("height/{id}")]
        public IHttpActionResult DeleteHeight(string id)
        {
            var measurementOwnerId = measurementService.GetMeasurementOwnerId<Height>(id);

            if (!userService.IsAuthorizedToAccess(idClaim.Value, measurementOwnerId))
                return Unauthorized();

            bool measurementWasDeleted = measurementService.Delete<Height>(id);
            if (measurementWasDeleted)
            {                
                return Ok();
            }
            
            return StatusCode(HttpStatusCode.BadRequest);
        }

        [HttpPut]
        [Route("height")]
        public IHttpActionResult UpdateHeight(Height measurement)
        {
            if (!userService.IsAuthorizedToAccess(idClaim.Value, measurement.PatientId))
                return Unauthorized();

            bool measurementWasUpdated = measurementService.Update<Height>(measurement);
            if (measurementWasUpdated)
            {                
                return Ok();
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        #endregion

        #region Spirometer

        [Route("spirometer")]
        [HttpPost]
        public IHttpActionResult PostSpirometer(Spirometer measurement, [FromUri] string patientId = null)
        {
            if (roleClaim.Value == "patient")
                patientId = idClaim.Value;

            if (!userService.IsAuthorizedToAccess(idClaim.Value, patientId))
                return Unauthorized();

            measurement.PatientId = patientId;

            var measurementWasInserted = measurementService.Create<Spirometer>(measurement);
            if (measurementWasInserted)
            {               
                return Ok();
            }

            return StatusCode(HttpStatusCode.BadRequest);
        }

        [HttpGet]
        [Route("spirometer")]
        public IHttpActionResult GetAllSpirometer([FromUri] string patientId = null)
        {
            if (roleClaim.Value == "patient")
                patientId = idClaim.Value;

            if (!userService.IsAuthorizedToAccess(idClaim.Value, patientId))
                return Unauthorized();

            var measurements = measurementService.GetPatientMeasurements<Spirometer>(patientId); 
            if (measurements != null)
            {
                return Ok(measurements);
            }
            
            return StatusCode(HttpStatusCode.NoContent);
        }

        [HttpDelete]
        [Route("spirometer/{id}")]
        public IHttpActionResult DeleteSpirometer(string id)
        {
            var measurementOwnerId = measurementService.GetMeasurementOwnerId<Spirometer>(id);

            if (!userService.IsAuthorizedToAccess(idClaim.Value, measurementOwnerId))
                return Unauthorized();

            bool measurementWasDeleted = measurementService.Delete<Spirometer>(id);
            if (measurementWasDeleted)
            {                
                return Ok();
            }
            
            return StatusCode(HttpStatusCode.BadRequest);
        }

        [HttpPut]
        [Route("spirometer")]
        public IHttpActionResult UpdateSpirometer(Spirometer measurement)
        {
            if (!userService.IsAuthorizedToAccess(idClaim.Value, measurement.PatientId))
                return Unauthorized();

            bool measurementWasUpdated = measurementService.Update<Spirometer>(measurement);
            if (measurementWasUpdated)
            {                
                return Ok();
            }

            return StatusCode(HttpStatusCode.BadRequest);
        }

        #endregion

        #region INR

        [Route("inr")]
        [HttpPost]
        public IHttpActionResult PostINR(INR measurement, [FromUri] string patientId = null)
        {
            if (roleClaim.Value == "patient")
                patientId = idClaim.Value;

            if (!userService.IsAuthorizedToAccess(idClaim.Value, patientId))
                return Unauthorized();

            measurement.PatientId = patientId;

            var measurementWasInserted = measurementService.Create<INR>(measurement);
            if (measurementWasInserted)
            {                
                return Ok();
            }

            return StatusCode(HttpStatusCode.BadRequest);
        }

        [HttpGet]
        [Route("inr")]
        public IHttpActionResult GetAllINR([FromUri] string patientId = null)
        {
            if (roleClaim.Value == "patient")
                patientId = idClaim.Value;

            if (!userService.IsAuthorizedToAccess(idClaim.Value, patientId))
                return Unauthorized();

            var measurements = measurementService.GetPatientMeasurements<INR>(patientId); 
            if (measurements != null)
            {
                return Ok(measurements);
            }
            
            return StatusCode(HttpStatusCode.NoContent);
        }

        [HttpDelete]
        [Route("inr/{id}")]
        public IHttpActionResult DeleteINR(string id)
        {
            var measurementOwnerId = measurementService.GetMeasurementOwnerId<INR>(id);

            if (!userService.IsAuthorizedToAccess(idClaim.Value, measurementOwnerId))
                return Unauthorized();

            bool measurementWasDeleted = measurementService.Delete<INR>(id);
            if (measurementWasDeleted)
            {               
                return Ok();
            }
            
            return StatusCode(HttpStatusCode.BadRequest);
        }

        [HttpPut]
        [Route("inr")]
        public IHttpActionResult UpdateINR(INR measurement)
        {
            if (!userService.IsAuthorizedToAccess(idClaim.Value, measurement.PatientId))
                return Unauthorized();

            bool measurementWasUpdated = measurementService.Update<INR>(measurement);
            if (measurementWasUpdated)
            {                
                return Ok();
            }

            return StatusCode(HttpStatusCode.BadRequest);
        }

        #endregion

        #region RespiratoryRate

        [Route("respiratoryrate")]
        [HttpPost]
        public IHttpActionResult PostRespiratoryRate(RespiratoryRate measurement, [FromUri] string patientId = null)
        {
            if (roleClaim.Value == "patient")
                patientId = idClaim.Value;

            if (!userService.IsAuthorizedToAccess(idClaim.Value, patientId))
                return Unauthorized();

            measurement.PatientId = patientId;
            var measurementWasInserted = measurementService.Create<RespiratoryRate>(measurement);
            if (measurementWasInserted)
            {                
                return Ok();
            }

            return StatusCode(HttpStatusCode.BadRequest);
        }

        [HttpGet]
        [Route("respiratoryrate")]
        public IHttpActionResult GetAllRespiratoryRate([FromUri] string patientId = null)
        {
            if (roleClaim.Value == "patient")
                patientId = idClaim.Value;

            if (!userService.IsAuthorizedToAccess(idClaim.Value, patientId))
                return Unauthorized();

            var measurements = measurementService.GetPatientMeasurements<RespiratoryRate>(patientId);
            if (measurements != null)
            {
                return Ok(measurements);
            }
            
            return StatusCode(HttpStatusCode.NoContent);
        }

        [HttpDelete]
        [Route("respiratoryrate/{id}")]
        public IHttpActionResult DeleteRespiratoryRate(string id)
        {
            var measurementOwnerId = measurementService.GetMeasurementOwnerId<RespiratoryRate>(id);

            if (!userService.IsAuthorizedToAccess(idClaim.Value, measurementOwnerId))
                return Unauthorized();

            bool measurementWasDeleted = measurementService.Delete<RespiratoryRate>(id);
            if (measurementWasDeleted)
            {                
                return Ok();
            }
            
            return StatusCode(HttpStatusCode.BadRequest);
        }

        [HttpPut]
        [Route("respiratoryrate")]
        public IHttpActionResult UpdateINR(RespiratoryRate measurement)
        {
            if (!userService.IsAuthorizedToAccess(idClaim.Value, measurement.PatientId))
                return Unauthorized();

            bool measurementWasUpdated = measurementService.Update<RespiratoryRate>(measurement);
            if (measurementWasUpdated)
            {
                return Ok();
            }
            
            return StatusCode(HttpStatusCode.BadRequest);
        }

        #endregion

        #region TUG

        [Route("tug")]
        [HttpPost]
        public IHttpActionResult PostTUG(TUG measurement, [FromUri] string patientId = null)
        {
            if (roleClaim.Value == "patient")
                patientId = idClaim.Value;

            if (!userService.IsAuthorizedToAccess(idClaim.Value, patientId))
                return Unauthorized();

            measurement.PatientId = patientId;

            var measurementWasInserted = measurementService.Create<TUG>(measurement);
            if (measurementWasInserted)
            {                
                return Ok();
            }

            return StatusCode(HttpStatusCode.BadRequest);
        }

        [HttpGet]
        [Route("tug")]
        public IHttpActionResult GetAllTUG([FromUri] string patientId = null)
        {
            if (roleClaim.Value == "patient")
                patientId = idClaim.Value;

            if (!userService.IsAuthorizedToAccess(idClaim.Value, patientId))
                return Unauthorized();

            var measurements = measurementService.GetPatientMeasurements<TUG>(patientId); 
            if (measurements != null)
            {
                return Ok(measurements);
            }
            
            return StatusCode(HttpStatusCode.NoContent);
        }

        [HttpDelete]
        [Route("tug/{id}")]
        public IHttpActionResult DeleteTUG(string id)
        {
            var measurementOwnerId = measurementService.GetMeasurementOwnerId<TUG>(id);

            if (!userService.IsAuthorizedToAccess(idClaim.Value, measurementOwnerId))
                return Unauthorized();

            bool measurementWasDeleted = measurementService.Delete<TUG>(id);
            if (measurementWasDeleted)
            {                
                return Ok();
            }
            
            return StatusCode(HttpStatusCode.BadRequest);
        }

        [HttpPut]
        [Route("tug")]
        public IHttpActionResult UpdateTUG(TUG measurement)
        {
            if (!userService.IsAuthorizedToAccess(idClaim.Value, measurement.PatientId))
                return Unauthorized();

            bool measurementWasUpdated = measurementService.Update<TUG>(measurement);
            if (measurementWasUpdated)
            {                
                return Ok();
            }

            return StatusCode(HttpStatusCode.BadRequest);
        }

        #endregion

        #endregion

        #region Private Methods
  
        #endregion
    }
}

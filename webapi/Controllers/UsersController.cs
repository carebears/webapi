﻿/* 
 * Copyright 2014 Aarhus University
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;
using DTO;
using Microsoft.AspNet.Identity;
using WebAPI.Services;

namespace WebAPI.Controllers
{
    /// <summary>
    /// When the UserController is called the web api tries to find an action, Web API looks at the HTTP method, and then looks for an action whose name begins with that HTTP method name. For example, with a GET request, Web API looks for an action that starts with "Get...", such as "GetContact" or "GetAllContacts".
    /// With insperation from http://www.asp.net/web-api/overview/creating-web-apis/creating-a-web-api-that-supports-crud-operations
    /// </summary>
    [RoutePrefix("api/users")]
    public class UsersController : ApiController
    {
        #region Private Fields

        private UserService<User> userService;
        private UserService<Patient> patientService;
        private UserService<HealthcareProfessional> hcpService;
        private Claim idClaim;
        private Claim roleClaim;

        #endregion

        #region Constructors
       
        public UsersController()
        {
            userService = new UserService<User>();
            patientService = new UserService<Patient>();
            hcpService = new UserService<HealthcareProfessional>();

            var identity = User.Identity as ClaimsIdentity;
            if (identity != null && identity.Claims.Count() != 0)
            {
                idClaim = identity.Claims.First(c => c.Type == "id");
                roleClaim = identity.Claims.First(c => c.Type == "role");
            }
            
        }

        #endregion 

        #region Public Methods

        #region User
        /// <summary>
        /// Gets authorized user.
        /// </summary>
        /// <returns>HTTP 200: Authorized user in body</returns>
        /// <returns>HTTP 404: User not found.</returns>
        [Authorize]
        [HttpGet]
        public IHttpActionResult GetUser()
        {
            User user;
            switch (roleClaim.Value)
            {
                case "patient":
                    user = patientService.Read(idClaim.Value);
                    break;
                case "healthcareprofessional":
                    user = hcpService.Read(idClaim.Value);
                    break;
                default:
                    user = userService.Read(idClaim.Value);
                    break;
            }

            if (user == null)
                return NotFound();
      
            return Ok(user);
        }

        /// <summary>
        /// Creates a new user if it does not already exist in the database.
        /// </summary>
        /// <param name="user"></param>
        /// <returns>Http response message about wether the user was created in the database.</returns>
        [AllowAnonymous]
        [HttpPost]
        public IHttpActionResult PostUser(User user)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var result = userService.Create(user);
            var errorResult = GetErrorResult(result);

            if (errorResult != null)
                return errorResult;

            return Ok();
        }

        /// <summary>
        /// Deletes a specified user from the database.
        /// </summary>
        /// <param name="userToDelete"></param>
        /// <returns>Http response message about wether the user was deleted in the database</returns>
        [Authorize]
        [HttpDelete]
        [Route("{id}")]
        public IHttpActionResult DeleteUser(string id)
        {
            if (!userService.IsAuthorizedToAccess(idClaim.Value, id))
                return Unauthorized();

            bool wasDeleted = userService.Delete(id);
            if (!wasDeleted)
                return BadRequest();

            return Ok();
        }

        /// <summary>
        /// Updates/replaces a user that allready exists with new properties.
        /// </summary>
        /// <param name="user"></param>
        /// <returns>Http response message about wether the user was updated.</returns>
        [Authorize]
        [HttpPut]
        public IHttpActionResult PutUser(User user)
        {
            if (!userService.IsAuthorizedToAccess(idClaim.Value, user.Id))
                return Unauthorized();

            bool userWasUpdated = userService.Update(user);
            if (!userWasUpdated)
                return BadRequest();

            return Ok();
        }
        #endregion

        #region Patient

        /// <summary>
        /// Creates a new patient if it does not already exist in the database.
        /// </summary>
        /// <param name="user"></param>
        /// <returns>Http response message about wether the user was created in the database.</returns>
        [AllowAnonymous]
        [Route("patient")]
        [HttpPost]
        public async Task<IHttpActionResult> PostPatient(Patient patient)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            patient.AddRole("patient");

            var result = patientService.Create(patient);
            var errorResult = GetErrorResult(result);

            if (errorResult != null)
                return errorResult;

            return Ok();
        }

        #endregion

        #region Healthcare Professional

        /// <summary>
        /// Creates a new Healthcare Professional if it does not already exist in the database.
        /// </summary>
        /// <param name="user"></param>
        /// <returns>Http response message about wether the Healthcare Professional was created in the database.</returns>
        [AllowAnonymous]
        [Route("healthcareprofessional")]
        [HttpPost]
        public async Task<IHttpActionResult> PostHealthcareProfessional(HealthcareProfessional healthcareProfessional)
        {
            healthcareProfessional.AddRole("healthcareprofessional");
            if (healthcareProfessional.Patients == null)
                healthcareProfessional.Patients = new List<string>();
            
            var result = hcpService.Create(healthcareProfessional);
            var errorResult = GetErrorResult(result);

            if (errorResult != null)
                return errorResult;

            return Ok();
        }

        #endregion

        #endregion

        #region Private Methods

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }

        #endregion

    }
}

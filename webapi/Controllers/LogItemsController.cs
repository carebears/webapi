﻿/* 
 * Copyright 2014 Aarhus University
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Web.Http;
using DTO;
using WebAPI.Services;

namespace WebAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/logitems")]
    public class LogItemsController : ApiController
    {
        #region Private Fields

        private string currentUserId;
        private LogService logs;

        #endregion

        #region Constructors

        LogItemsController()
        {
            var identity = User.Identity as ClaimsIdentity;
            currentUserId = identity.Claims.FirstOrDefault(c => c.Type == "id").Value;
            var fullUserName = identity.Claims.FirstOrDefault(c => c.Type == "fullname");
            logs = new LogService(currentUserId, fullUserName.Value);
        }

        #endregion
        
        #region Public Methods

        /// <summary>
        /// This function gets all log items that belongs to the current user that is less then 30 days old
        /// </summary>
        /// <returns>List of logitems containing info on WHICH measurement have recieved WHAT kind of change by WHO and WHEN</returns>
        [HttpGet]
        public IHttpActionResult GetAllLogItems()
        {
            List<LogItem> logItemList = logs.GetAllUserLogs(30);

            if (logItemList != null)
            {               
                logItemList.OrderByDescending(x => x.Date);
                return Ok(logItemList.ToList());
            }
            else
            {
                return StatusCode(HttpStatusCode.BadRequest);
            }
            return null;
        }

        /// <summary>
        /// This function gets all log items that belongs to the current user that is less then the age specified in the argument
        /// </summary>
        /// <returns>List of logitems containing info on WHICH measurement have recieved WHAT kind of change by WHO and WHEN</returns>
        [HttpGet]
        [Route("{maxLogAge}")]
        public IHttpActionResult GetAllLogItems(Int32 maxLogAge)
        {
            List<LogItem> logItemList = logs.GetAllUserLogs(maxLogAge);
            
            if (logItemList != null)
            {        
                logItemList.OrderByDescending(x => x.Date);
                return Ok(logItemList.ToList());
            }
            else
            {
                return StatusCode(HttpStatusCode.BadRequest);
            }
            return null;
        }
        #endregion
    }
}

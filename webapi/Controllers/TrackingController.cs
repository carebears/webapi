/* 
 * Copyright 2014 Aarhus University
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System.Linq;
using System.Security.Claims;
using System.Web.Http;
using DTO;
using WebAPI.Services;

namespace WebAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/tracking")]
    public class TrackingController : ApiController
    {
        #region Private fields

        private readonly Claim idClaim;
        private readonly Claim roleClaim;
        private ClaimsIdentity identity;
        private ITrackingService trackingService;
        private IUserService<User> userService;

        #endregion

        #region Constructors

        public TrackingController()
        {
            identity = User.Identity as ClaimsIdentity;
            idClaim = identity.Claims.FirstOrDefault(c => c.Type == "id");
            roleClaim = identity.Claims.FirstOrDefault(c => c.Type == "role");
            trackingService = new TrackingService();
            userService = new UserService<User>();
        }

        #endregion

        #region Patients

        [HttpGet]
        [Route("patients/{id}")]
        public IHttpActionResult GetPatient(string id)
        {
            if (!userService.IsAuthorizedToAccess(idClaim.Value, id))
                return Unauthorized();

            var patient = trackingService.GetPatientById(id);

            return Ok(patient);
        }

        #endregion

        #region Tracked Patients

        [HttpGet]
        [Route("patients")]
        public IHttpActionResult GetAllTrackedPatients()
        {
            if (roleClaim.Value == "patient")
                return Unauthorized();

            var patients = trackingService.GetTrackedPatients(idClaim.Value);

            return Ok(patients);
        }

        [HttpPost]
        [Route("patients")]
        public IHttpActionResult PostTrackedPatient([FromUri] string email, [FromUri] string passphrase)
        {
            if (roleClaim.Value == "patient")
                return Unauthorized();

            if (!trackingService.AddPatientTracking(idClaim.Value, email, passphrase))
                return BadRequest();

            return Ok();
        }

        [HttpDelete]
        [Route("patients/{id}")]
        public IHttpActionResult DeleteTrackedPatient(string id)
        {
            if (roleClaim.Value == "patient")
                return Unauthorized();

            if (!userService.IsAuthorizedToAccess(idClaim.Value, id))
                return NotFound();

            if (!trackingService.RemovePatientTracking(idClaim.Value, id))
                return BadRequest();

            return Ok();
        }

        #endregion

        #region Healthcare Professionals
        [HttpGet]
        [Route("healthcareprofessionals")]
        public IHttpActionResult GetAllTrackingHealthcareProfessionals()
        {
            if (roleClaim.Value != "patient")
                return Unauthorized();

            var trackingHcpList = trackingService.GetTrackingHealthcareProfessionals(idClaim.Value);

            return Ok(trackingHcpList);
        }
        #endregion

        #region Passphrase
        [HttpPost]
        [Route("patients/passphrase")]
        public IHttpActionResult PostPassphrase([FromBody] string passphrase)
        {
            if (roleClaim.Value != "patient")
                return Unauthorized();

            trackingService.CreatePassphrase(idClaim.Value, passphrase);
            
            return Ok();

        }
        #endregion
    }
}
﻿/* 
 * Copyright 2014 Aarhus University
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Threading.Tasks;
using AspNet.Identity.MongoDB;
using Database;
using DTO;
using Microsoft.AspNet.Identity;

namespace WebAPI.Security
{
    public class AuthRepository<T> : IDisposable where T : IdentityUser, IDTO 
    {
        private readonly IDatabaseHandler<T> _userDatabaseHandler;
        private readonly IDatabaseHandler<RefreshToken> _tokenDatabaseHandler;
        private readonly UserManager<T> _userManager;

        public AuthRepository()
        {
            _userDatabaseHandler = new DatabaseHandler<T>();
            _tokenDatabaseHandler = new DatabaseHandler<RefreshToken>();
            _userManager = new UserManager<T>(new UserStore<T>(new IdentityContext(_userDatabaseHandler.Collection)));
        }

        public IdentityResult RegisterUser(T user, string password)
        {
            user.Email = user.Email.ToLower();
            user.UserName = user.UserName.ToLower();
            var result = _userManager.Create(user, password);
            return result;
        }

        public async Task<T> FindUser(string userName, string password)
        {
            var user = (T) await _userManager.FindAsync(userName.ToLower(), password);

            return user;
        }

        public void Dispose()
        {
            _userManager.Dispose();
        }
    }
}
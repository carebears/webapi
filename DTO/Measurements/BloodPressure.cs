/* 
 * Copyright 2014 Aarhus University
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System.Collections.Generic;

namespace DTO.Measurements
{
    public class BloodPressure : Measurement
    {
        public float Systolic { get; set; }
        public float Diastolic { get; set; }
        public float HeartRate { get; set; }
       
        public BloodPressure()
        {
            Unit = new List<KeyValuePair<string, string>>();
            Unit.Add(new KeyValuePair<string, string>("Systolic", "mmHg"));
            Unit.Add(new KeyValuePair<string, string>("Diastolic", "mmHg"));
            Unit.Add(new KeyValuePair<string, string>("HeartRate", "BPM"));                                               
        }
    }
}
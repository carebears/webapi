﻿/* 
 * Copyright 2014 Aarhus University
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;

namespace DTO
{
    public class LogItem : IDTO
    {
        [JsonIgnore]
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [JsonIgnore]
        [BsonRepresentation(BsonType.ObjectId)]
        public string measurementId { get; set; }

        [JsonIgnore]
        [BsonRepresentation(BsonType.ObjectId)]
        public string UserId { get; set; }                  //Author of measurement CRUD
                
        [JsonIgnore]
        public string MeasurementOwnerId { get; set; }      //Measurement owner
                             
        public string MeasurementType { get; set; }         
        public string Action { get; set; }               
        public string UserModifyedBy { get; set; }          //Name of Author of measurement CRUD
        public DateTime Date { get; set; }

    }
}
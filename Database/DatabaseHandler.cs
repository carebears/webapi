﻿/* 
 * Copyright 2014 Aarhus University
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using DTO;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;

namespace Database
{
    public class DatabaseHandler<Mongo> : IDatabaseHandler<Mongo> where Mongo : IDTO
    {
        #region Properties

        public MongoCollection<Mongo> Collection
        {
            get { return mongoCollection; }
        }

        #endregion

        #region Private fields

        private MongoDatabase database;
        private MongoCollection<Mongo> mongoCollection;

        #endregion

        #region Constructors

        public DatabaseHandler()
        {
            GetDatabase();
            GetCollection();
        }

        #endregion

        #region Public methods

        public bool Insert(Mongo entity)
        {
            entity.Id = ObjectId.GenerateNewId().ToString();

            return mongoCollection.Insert(entity).Ok;
        }

        public bool Delete(Mongo entity)
        {
            //Second argument in Query.EQ() was changed from "entity.Id" to "new ObjectId(entity.Id))" because the type needs to be ObjectId
            return mongoCollection.Remove(Query.EQ("_id", new ObjectId(entity.Id))).DocumentsAffected > 0;
        }

        public bool Update(Mongo entity)
        {
            return mongoCollection.Save(entity).DocumentsAffected > 0;

        }

        public IList<Mongo> SearchFor(Expression<Func<Mongo, bool>> predicate)
        {
            
            return mongoCollection
                .AsQueryable<Mongo>()
                    .Where(predicate.Compile()).ToList();

        }

        public IList<Mongo> GetAll()
        {
            return mongoCollection.FindAllAs<Mongo>().ToList();
        }

        public Mongo GetById(string id)
        {
            return mongoCollection.FindOneByIdAs<Mongo>(new ObjectId(id));
        }


        #endregion

        #region Private methods

        private void GetDatabase()
        {
            var client = new MongoClient(GetConnectionString());
            var server = client.GetServer();

            database = server.GetDatabase(GetDatabaseName());
        }

        private string GetDatabaseName()
        {
            return ConfigurationManager
                .AppSettings
                .Get("DatabaseName");
        }

        private string GetConnectionString()
        {
            return ConfigurationManager
                .AppSettings
                .Get("DatabaseConnectionString")
                .Replace("{DB_NAME}", GetDatabaseName());
        }

        private void GetCollection()
        {
            mongoCollection = database
                .GetCollection<Mongo>(typeof(Mongo).Name);
        }

        #endregion
    }
}
